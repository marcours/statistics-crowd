import datetime as dt
import json

import matplotlib.pyplot as plt
import numpy as np
from geopy.distance import vincenty

from mastodonte_plot import get_history_json


def get_geo_id(position):
    """
    Out id 0

    Lidia id 1
    McLab id 2
    Parchetto id 3

    :param position: Test point to check if it's into any Geofence
    :return: The Geofence id
    """
    radius = 32

    parchetto_geo_center = (39.228412, 9.109770)
    mclab_geo_center = (39.229199, 9.109688)
    lidia_geo_center = (39.230092, 9.107767)

    lat_test, lon_test = str(position).split(",")

    test_point = (lat_test, lon_test)

    distance = vincenty(test_point, parchetto_geo_center).meters
    if distance < radius:
        print "Parchetto id 3"
        return 3
    elif vincenty(test_point, mclab_geo_center).meters < radius:
        print "McLab id 2"
        return 2
    elif vincenty(test_point, lidia_geo_center).meters < radius:
        print "Lidia id 1"
        return 1
    else:
        "Out id 0"
        return 0
    pass


vo_ids = np.arange(0, 19, 1)
data = []
file = [0] * len(vo_ids)
limit = 200
h_start = 10
m_start = 10
h_stop = 20
m_stop = 00
day = 28
month = 06

for v_id in vo_ids:
    if v_id != 11:
        file[v_id] = get_history_json(vo_id=v_id, h_start=h_start,
                                      m_start=m_start,
                                      h_stop=h_stop, m_stop=m_stop, day=day)
        print "History of svo {} received\n".format(v_id)

for v_id in vo_ids:
    if v_id != 11:
        fname = "history_{}_device{}.json".format(day, v_id)
        with open(fname, 'w') as outfile:
            json.dump(file[v_id], outfile)

for v_id in vo_ids:
    if v_id != 11:
        fname = "history_{}_device{}.json".format(day, v_id)
        with open(fname) as data_file:
            data.append(json.load(data_file))

data = np.array(data)

print data
bl_list = []
t_list = []
sc_list = []
ps_list = []
geo_id_list = []
hours_counter = np.zeros(shape=(18,24, 4))
minutes_counter = np.zeros(shape=(18, 24, 60, 4))

for index, ele in enumerate(data):
    bl = []
    t = []
    sch = []
    ps = []
    geo_id = []
    print "Length di ele {} is {}".format(index, len(ele))
    for sample in ele:
        bl.append(sample['battery-level'])
        t.append(sample['timestamp'])
        sch.append(sample['schedule'])
        ps.append(sample['position'])
        geo_id.append(get_geo_id(position=sample['position']))


    t_list = [dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S") for elem in
              t]

    geo_id = np.array(geo_id)
    for i, el in enumerate(t_list):
        hours_counter[index, el.hour, geo_id[i]] = 1
        minutes_counter[index, el.hour, el.minute ,geo_id[i]] = 1

    t_list.append(t)
    bl_list.append(bl)
    sc_list.append(sch)
    ps_list.append(ps)
    geo_id_list.append(geo_id)

    # x = [dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S") for elem in t]
    # plt.plot(x, bl)
    # plt.title('Merda')


dy = np.sum(hours_counter,axis=0)

x = np.arange(1,dy.shape[0]+1,1)
plt.plot(x,dy)

# plt.show()
print t_list
t_list = np.array(t_list)

x = np.arange(1,dy.shape[0]+1,1)

# plt.subplot(4,1,1)
# plt.title('Geo id 0')
# plt.scatter(x,dy[:,0])
# plt.plot(x,dy[:,0])
#
# plt.subplot(4,1,2)
# plt.title('Geo id 1')
# plt.scatter(x,dy[:,1])
# plt.plot(x,dy[:,1])
#
# plt.subplot(4,1,3)
# plt.title('Geo id 2')
# plt.scatter(x,dy[:,2])
# plt.plot(x,dy[:,2])
#
# plt.subplot(4,1,4)
# plt.title('Geo id 3')
# plt.scatter(x,dy[:,3])
# plt.plot(x,dy[:,3])
#
#
#
# plt.show()