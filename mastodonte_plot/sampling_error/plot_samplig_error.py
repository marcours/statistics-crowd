import datetime as dt
import json

import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':

    data = []
    fname = "/Users/marcouras/PycharmProjects/statistics_crowd/frequency_error/raimondo_26.json"
    with open(fname) as data_file:
        data.append(json.load(data_file))

    t = []
    i = 0
    delta_t = []
    data = data[0]['Entries']
    bl = []
    t_bl = []
    for sample in data:
        # print sample
        if sample['name'] == 'BATTERY-LEVEL':
            bl.append(sample['value'])
            t_bl.append(dt.datetime.strptime(sample['timestamp'],
                                             '%a %b %d %H:%M:%S %Z %Y'))

        t.append(dt.datetime.strptime(sample['timestamp'],

                                      '%a %b %d %H:%M:%S %Z %Y'))
    for t in t_bl:
        if i > 1:
            delta = (t_bl[i - 1] - t_bl[i])
            print delta.seconds
            # dt.timedelta.seconds
            if (delta.seconds < 500) and (delta.seconds != 0):
                delta_t.append(delta.seconds)
        i += 1

    delta_t = np.array(delta_t)
    delta_t = delta_t[0:149]
    x = np.arange(0, len(delta_t), 1)

    plt.subplot(3, 1, 1)
    plt.plot(x, delta_t)
    plt.title('Schedule Period')

    freq = []
    for d in delta_t:
        print d
        freq.append(1.0 / d)

    plt.subplot(3, 1, 2)
    plt.plot(x, freq)
    plt.title('Frequency')

    error = []
    for d in delta_t:
        print 1 - (300.0 / d)
        error.append(1 - (300.0 / d))

    plt.subplot(3, 1, 3)
    plt.plot(x, error)
    plt.title('Frequency Error')
    plt.show()

    error_sub = np.array(error)
    error_sub = error_sub[0:60]

    error_mean = np.array(error_sub).mean()
    error_std = np.var(error_sub)

    print "Mean error is {} +/- {}".format(error_mean, error_std)

    x_sub = np.arange(0, len(error_sub), 1)

    plt.plot(x_sub, error_sub)
    plt.show()
