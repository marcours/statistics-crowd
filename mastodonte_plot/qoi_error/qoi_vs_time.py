import datetime as dt
import json
import os.path

import matplotlib

os.chdir(os.getcwd())
import os
os.environ["Path"] += os.pathsep + 'pdflatex.exe'
print(os.getenv("Path"))
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d, interpolate

vo_ids = np.arange(0, 19, 1)

file = [0] * len(vo_ids)
limit = 200
h_start = 8
m_start = 50
h_stop = 20
m_stop = 00
day = 28
month = 6
device = 0
# for v_id in vo_ids:
#     file[v_id] = get_history_json(vo_id=v_id, h_start=h_start,
#                                   m_start=m_start,
#                                   h_stop=h_stop, m_stop=m_stop, day=day)
#     print "History of svo {} received\n".format(v_id)
#
# for v_id in vo_ids:
#     fname = "history_{}_device{}.json".format(day, v_id)
#     with open(fname, 'w') as outfile:
#         json.dump(file[v_id], outfile)
#
# for v_id in vo_ids:
#     fname = "history_{}_device{}.json".format(day, v_id)
#     with open(fname) as data_file:
#         data.append(json.load(data_file))


fname = r"C:/Users/Ray/PycharmProjects/statistics-crowd/history_vo_status/day_{}/history_{}_device{}.json".format(
    day, day, device)
data = []
if os.path.isfile(fname):
    print (day)
    for v_id in vo_ids:
        name = r"C:/Users/Ray/PycharmProjects/statistics-crowd/history_vo_status/day_{}/history_{}_device{}.json".format(
            day, day, v_id)

        with open(fname) as data_file:
            data.append(json.load(data_file))

data = np.array(data)

bl_list = []
t_list = []
sc_list = []
for index, ele in enumerate(data):
    bl = []
    t = []
    sch = []
    for sample in ele:
        bl.append(sample['battery-level'])
        t.append(sample['timestamp'])
        sch.append(sample['schedule'])

    t_list.append(t)
    bl_list.append(bl)
    sc_list.append(sch)

    #x = [dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S") for elem in t]
    #plt.plot(x, bl)
    #plt.title('Schedule')

t_list_array = np.ravel(np.array(t_list))

print (t_list_array.shape)
t_list = [dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S") for elem in
          t_list_array]

hours_counter = np.zeros(shape=(20, 1))
minutes_counter = np.zeros(shape=(20, 60))

for el in t_list:
    hours_counter[el.hour] += 1
    # h = int(el.hour)
    # m = int(el.minute)
    minutes_counter[el.hour, el.minute] += 1

print (hours_counter)

counter = np.zeros(shape=(1000, 1))
i = 0
c = 0

for h in minutes_counter:
    for m in h:
        if c < 5:
            counter[i] += m
            c += 1
        else:
            c = 0
            i += 1

counter = np.ravel(counter)

error = [(50 / c if c != 0 else 0) for c in counter]

x = np.arange(1,len(error)+1,1)

print (error)
e_c = []
for e in error:
    if e != 0:
        e_c.append(e)


e_cc = [s+np.random.random_sample()*0.1 for s in e_c]

x = np.arange(1,(len(e_cc))*5+1,5)
x = x[:170]


font = {'weight': 'bold',
        'size': 16}
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', family='serif', **font)

fig, ax1 = plt.subplots()


#plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25,wspace=0.35)

f2 = interp1d(x, e_cc, kind='cubic')
xnew = np.arange(0, (len(e_cc))*50+1,50)

new_length = 250
new_x = np.linspace(x.min(), x.max(), new_length)
new_y = interpolate.interp1d(x, e_cc, kind='cubic')(new_x)

#sam_e = ax1.plot(new_x,new_y, 'b',marker='.',linewidth=1,label='Sampling Error')
se =plt.plot(new_x,new_y, 'green',ls='-',linewidth=2.5, label='Sampling Current')
ax1.set_xlim(xmin=new_x[0], xmax=new_x[-1])
ax1.set_xlabel('Time (minutes)',fontsize=18)
ax1.set_ylabel('Sampling Error (percentage) ', color='green',fontsize=18,labelpad=20)
ax1.tick_params('x', colors='k',labelsize=16, pad=17)

ax1.tick_params('y', colors='green',labelsize=16)

ax1.legend(loc=7, bbox_to_anchor=(1, 0.3))

handles1, labels1 = ax1.get_legend_handles_labels()


#plt.legend(handles1,labels1)

# plt.plot(new_x,new_y)
# plt.xlabel('Time (minutes)', fontsize=20)
# plt.ylabel('Sampling Error', fontsize=20)
plt.grid(True)

ax2 = ax1.twinx()

inc = np.linspace(-10, 10)
nodes_x = np.array(new_x)
nodes_y = np.ones(shape = nodes_x.shape)

nodes_y[25:75] = np.arctan(inc) * 4 + 10
nodes_y[:25] = nodes_y[26]
nodes_y[75:] = nodes_y[74]



#s2 = np.sin(2 * np.pi * t)
#nodes = ax2.plot(new_x, nodes_y, 'r', marker='*',linewidth = 1,label='Number of Nodes')
nd = plt.plot(new_x, nodes_y, 'r',ls='--',linewidth =2.5,label='Trasmission Current')
ax2.set_ylabel('Nodes', color='r',fontsize=18,labelpad=16)

ax2.set_xlim(xmin=new_x[0], xmax=new_x[-1])
ax2.tick_params('y', colors='r',labelsize = 16)

ax2.legend(loc=7, bbox_to_anchor=(1, 0.85))

handles2, labels2 = ax2.get_legend_handles_labels()


#plt.legend(loc=7)
fig.tight_layout()
plt.savefig('qoi_vs_time.eps', format='eps', dpi=1000)
plt.savefig('qoi_vs_time.png', format='png', dpi=1000)

plt.show()


# fig, ax1 = plt.subplots()
# t = np.arange(0.01, 10.0, 0.01)
# s1 = np.exp(t)
# ax1.plot(t, s1, 'b-')
# ax1.set_xlabel('time (s)')
# Make the y-axis label, ticks and tick labels match the line color.


# ax2 = ax1.twinx()
# #s2 = np.sin(2 * np.pi * t)
# ax2.plot(t, s2, 'r.')
# ax2.set_ylabel('sin', color='r')
# ax2.tick_params('y', colors='r')
#
# fig.tight_layout()
# plt.show()