import datetime as dt
import json

import matplotlib.pyplot as plt
import numpy as np

from mastodonte_plot import get_history_json

vo_ids = np.arange(0, 19, 1)


data = []
file = [0]*len(vo_ids)
limit = 600
h_start = 9
m_start = 07
h_stop = 20
m_stop = 00
day = 29
month = 06

fname = "history_{}_device{}.json".format(day, 0)
dfname = "day_{}/history_{}_device{}.json".format(day, day, 0)

# if os.path.isfile(dfname):
#     print "da_26"
#     for v_id in vo_ids:
#         dfname = "day_{}/history_{}_device{}.json".format(day, day, v_id)
#         with open(dfname) as data_file:
#             data[v_id] = json.load(data_file)
#
# elif os.path.isfile(fname):
#     print "project directory"
#     for v_id in vo_ids:
#         fname = "history_{}_device{}.json".format(day, v_id)
#         with open(fname) as data_file:
#             data[v_id] = json.load(data_file)
# else:
#     print "Files not found in project directory or day_{} directory".format(day)
#     for v_id in vo_ids:
#         data[v_id] = get_history_json(vo_id=v_id, h_start=h_start,
#                                       m_start=m_start,
#                                       h_stop=h_stop, m_stop=m_stop, day=day)
#         print "History of svo {} received\n".format(v_id)

#
for v_id in vo_ids:
    #if v_id != 0 or v_id != 3 or v_id != 15 or v_id != 11 or v_id != 4 or v_id != 9 or v_id != 18:
        file[v_id] = get_history_json(vo_id=v_id, h_start=h_start,
                                      m_start=m_start,
                                      h_stop=h_stop, m_stop=m_stop, day=day)
        print "History of svo {} received\n".format(v_id)

for v_id in vo_ids:
    #if v_id != 0 or v_id != 3 or v_id != 15 or v_id != 11 or v_id != 4 or v_id != 9 or v_id != 18:
        fname = "history_{}_device{}.json".format(day, v_id)
        with open(fname, 'w') as outfile:
            json.dump(file[v_id], outfile)

for v_id in vo_ids:
    #if v_id != 0 or v_id != 3 or v_id != 15 or v_id != 11 or v_id != 4 or v_id != 9 or v_id != 18:
        fname = "history_{}_device{}.json".format(day, v_id)
        with open(fname) as data_file:
            data.append(json.load(data_file))


data = np.array(data)

bl_list = []
t_list = []
sc_list = []
for index, ele in enumerate(data):
    bl = []
    t = []
    sch = []
    for sample in ele:
        bl.append(sample['battery-level'])
        t.append(sample['timestamp'])
        sch.append(sample['schedule'])

    t_list.append(t)
    bl_list.append(bl)
    sc_list.append(sch)

    x = [dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S") for elem in t]
    plt.plot(x, bl)
    plt.title('Battery Level')


t_list_array = np.ravel(np.array(t_list))

tot = np.array([])

plt.show()
