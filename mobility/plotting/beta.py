import numpy as np
from mobility.entities.Singleton_manager import SingletonManager
from matplotlib import pyplot as plt


def plot_beta():
    """

    n_fixed = 300       n_fixed = 350    n_fixed = 350
    dt = 10             dt = 1           dt = 0.1
    v_min = 1           v_min = 0.5      v_min = 0.1
    v:
    v_max = 20          v_max = 10       v_max = 10
    s     |  tau        s    |  tau      s    |  tau
    ---------------     --------------   --------------
    1     |  2355.33    1    |  729      1    |
    1.5   |  2357.14    1.5  |           1.5  |
    2     |  2365.33    2    |           2    |
    2.5   |  2370       2.5  |           2.5  |
    3     |  2380.66    3    |           3    |
    3.5   |  2370.0     3.5  |           3.5  |
    4     |  2374.67    4    |           4    |
    4.5   |  2357.85    4.5  |           4.5  |
    5     |  2368       5    |           5    |
    5.5   |  2354       5.5  |  667      5.5  |
    6     |  2354       6    |  000      6    |
    6.5   |  2368       6.5  |  000      6.5  |
    7     |  2354       7    |  000      7    |
    7.5   |  2362       7.5  |  000      7.5  |
    8     |  2363       8    |  000      8    |
    8.5   |  2363       8.5  |  000      8.5  |
    9     |  2365       9    |  000      9    |
    9.5   |  2363       9.5  |  000      9.5  |
    10    |  2363       10   |  000      10   |

    Dk = 100 m  Fk = 0.23 Hz

    Si = beta * Dk * Fk

    """

    beta = np.arange(0.04347, 0.4347, 0.0217)

    singl_man = SingletonManager()

    x_axis = singl_man.dead_time
    c = 0
    y_axis = np.zeros(x_axis.__len__())
    for i in range(x_axis.__len__()):
        c += 1
        y_axis[i] = c

    tau = np.mean(x_axis)
    print ("tau {}".format(tau))
    plt.title("Number of dead Devices")
    plt.plot(x_axis,y_axis, 'o', c='g')
    plt.show()