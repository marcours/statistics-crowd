import numpy as np
from mobility.entities.Singleton_manager import SingletonManager
from matplotlib import pyplot as plt


def plot_battery():
    singl_man = SingletonManager()

    x_axis = np.arange(singl_man.devices_battery.shape[1])

    y_axis = singl_man.devices_battery


    for i, bl in enumerate(y_axis):
        plt.plot(x_axis, bl)

    plt.grid()
    plt.title("Battery Level")
