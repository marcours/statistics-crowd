from mobility.entities import Node
from mobility.entities.Singleton_manager import SingletonManager
import numpy as np

def not_cool_task_frequencies():

    sing_man = SingletonManager()
    node_list = sing_man.get_sensing_nodes()

    F_k = 20#sing_man.qoi
    N_k = float(len(node_list))
    R = F_k
    print R

    c = 0
    for fx in sing_man.fixed_nodes:
        if fx.batt_level < 0:
            fx.batt_level = 0
        c += fx.batt_level

    stopper = N_k / c

    tx_costs = [n.tx_cost/n.batt_level for n in node_list]

    tx_costs = np.array(tx_costs)

    min_tx_costs = np.argpartition(tx_costs,R)

    for idx in range(R):
        if sing_man.time >1000:
            tx = node_list[min_tx_costs[idx]].tx_cost
            node_list[min_tx_costs[idx]].batt_level -= tx
            print node_list[min_tx_costs[idx]].batt_level

if __name__ == '__main__':


    from geopy import Point

    sing_manager = SingletonManager()
    center_lat = 39.228389
    center_lon = 9.109773
    center_point = Point(center_lat, center_lon)
    radius = 50 / 1000.0
    boundary = radius * 1.414
    n_geofence_points = 300
    n_mobile_nodes = 5
    n_fixed_nodes = 3

    sing_manager.qoi = 20  # Hz
    sing_manager.beta = 0.6
    sing_manager.center_point = center_point
    sing_manager.radius = radius
    sing_manager.n_fixed = n_fixed_nodes
    sing_manager.n_mobile = n_mobile_nodes

    sing_manager.generate_mobile_nodes(n_mobile_nodes, center_lat, center_lon)
    sing_manager.generate_fixed_node(10)

    are_sensing = [no.is_fixed for no in sing_manager.get_all_nodes()]



    for i, n in enumerate(sing_manager.get_all_nodes()):
        if are_sensing[i]:
            #print n
            n.is_sensing = True

    dt = 10
    t=0
    t_max = 1000
    while t < t_max:
        not_cool_task_frequencies()
        t += dt
        print t

    #for i in sing_manager.get_all_nodes():
        #print i