import json
from pprint import pprint
import matplotlib.dates as md
import numpy as np
import datetime as dt
import time
import requests
import matplotlib.pyplot as plt



if __name__ == '__main__':

    with open('frequency_error/ALL_SENSORS_ASUS.json') as data_file:
        data = json.load(data_file)

    data = data["Entries"]
    bl = []
    t = []

    for ele in data:
        bl.append(float(ele['valueMah']))
        t.append(ele['timestamp'])

    print (t)
    epoch = dt.datetime.utcfromtimestamp(0)

    # date_str = "2008-11-10 17:53:59"
    # dt_obj = dt.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")

    t_millis = [(dt.datetime.strptime(dat,"%Y-%m-%d %H:%M:%S" ) - epoch).total_seconds() * 1000.0
                for dat in t]
    print (t_millis)

    # dates = [dt.datetime.fromtimestamp(ts) for ts in t_millis]

    bl = np.array(bl)
    t = np.array(t)
    #b_drain = np.array(b_drain)

    # x = ['Mon Sep 1 16:40:20 2015', 'Mon Sep 1 16:45:20 2015',
    #   'Mon Sep 1 16:50:20 2015', 'Mon Sep 1 16:55:20 2015']
    # y = range(4)

    x = [dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S") for elem in t]


    plt.subplot(3, 1, 1)
    plt.plot(x, bl)
    plt.scatter(x,bl)
    plt.title('Battery Level')

    plt.subplot(3, 1, 2)
    plt.title('Battery Drain')
    plt.plot(x,b_drain)
    #plt.scatter(x,b_drain)


    plt.subplot(3, 1, 3)
    plt.title('Schedule Interval')
    #plt.plot(x, schedule_interv)
    #plt.scatter(x,schedule_interv)

    plt.show()


    # plt.subplots_adjust(bottom=0.2)
    # plt.xticks(rotation=25)
    # ax = plt.gca()
    # xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    # ax.xaxis.set_major_formatter(xfmt)
    # plt.plot(t_millis, bl)
    # plt.show()
