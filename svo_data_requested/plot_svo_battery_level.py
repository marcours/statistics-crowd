import json
from pprint import pprint
import matplotlib.dates as md
import matplotlib.patches as mpatches
import numpy as np
import datetime as dt
import time
import requests
import matplotlib.pyplot as plt
import matplotlib
import os
os.environ["Path"] += os.pathsep + 'pdflatex.exe'
print(os.getenv("Path"))

from svo_data_requested.get_svo_history import get_svo_history_json


def download_values(sensor, st_time, sp_time, ref_day, ref_h_stop):
    # data = get_svo_history_json(vo_id=vo, sensor=sensor,start_time=st_time,stop_time=sp_time)

    k = 'value'
    data = {}
    if sensor == 'BATTERY-LEVEL':
        k = 'valueMah'
        fname = "/Users/marcouras/PycharmProjects/statistics_crowd/svo_data_requested/static_data/batt77.json"
        with open(fname) as data_file:
            data = (json.load(data_file))
    elif sensor == 'BATTERY-DRAIN':
        fname = "/Users/marcouras/PycharmProjects/statistics_crowd/svo_data_requested/static_data/drain77.json"
        with open(fname) as data_file:
            data = (json.load(data_file))

    pprint(data)
    batt_values = []
    timestamp = []

    for ele in data['Entries']:
        #print(ele)
        # b_drain.append(float(ele['battery-drain'.upper()]))
        dtemp = dt.datetime.strptime(ele['timestamp'], "%a %b %d %H:%M:%S %Z %Y")

        if dtemp.day == ref_day and dtemp.hour < ref_h_stop:
            batt_values.append(float(ele[k]))
            timestamp.append(ele['timestamp'])

    return batt_values, timestamp


if __name__ == '__main__':
    """
    date format 2018 Mar 23 8:24:56
    """
    year = 2018
    h_start = 8
    m_start = 30
    h_stop = 21
    m_stop = 30
    day = 21
    month = "Mar"

    start_time = "{} {} {} {}:{}:00".format(year, month, day, h_start, m_start)
    stop_time = "{} {} {} {}:{}:00".format(year, month, day, h_stop, m_stop)
    vo = "lysis-77"

    font = {'weight': 'bold',
            'size': 18}

    #matplotlib.rc('font', **font)
    matplotlib.rc('text', usetex=True)
    matplotlib.rc('font', family='serif', **font)

    bl, t = download_values(sensor="BATTERY-LEVEL",
                            st_time=start_time, sp_time=stop_time,
                            ref_day=day, ref_h_stop= h_stop)

    print (t)

    epoch = dt.datetime.utcfromtimestamp(0)

    t_millis = [(dt.datetime.strptime(dat, "%a %b %d %H:%M:%S %Z %Y") - epoch)
                    .total_seconds() * 1000.0
                for dat in t]
    print (t_millis)

    bl = np.array(bl)
    t = np.array(t)

    x = [dt.datetime.strptime(elem, "%a %b %d %H:%M:%S %Z %Y") for elem in t]

    # plt.subplot(2, 1, 1)
    plt.plot(x, bl, color='red')
    plt.scatter(x, bl, label='Measured', color='red')
    plt.title('Battery Level')

    # Costruzione della stima

    b_drain, t_drain = download_values(sensor="BATTERY-DRAIN",
                                       st_time=start_time, sp_time=stop_time,
                                       ref_day=day, ref_h_stop=h_stop)

    t_millis = [(dt.datetime.strptime(dat, "%a %b %d %H:%M:%S %Z %Y") - epoch)
                    .total_seconds() / 3600.0
                for dat in t_drain]

    print (t_millis)
    x = [dt.datetime.strptime(elem, "%a %b %d %H:%M:%S %Z %Y") for elem in
         t_drain]

    forecast = np.zeros(shape=len(t_millis))
    forecast[0] = bl[-1]
    t_millis = np.array(t_millis)
    t_millis.sort()
    for idx, istant in enumerate(t_millis[1:]):
        delta = istant - t_millis[idx]
        forecast[idx + 1] = forecast[idx] - delta * b_drain[idx] * 0.65

    print (forecast)

    reverse_order = np.sort(forecast)[:-1]
    print (reverse_order)

    reverse_order[0] = reverse_order[1] * 0.5

    '''
    plt.subplot(2, 1, 2)
    plt.title('Battery Drain')
    plt.plot(x, b_drain)
    plt.scatter(x, b_drain)
    '''

    # plt.subplot(2, 1, 1)
    plt.title('Battery Level')
    plt.plot(x[1:], reverse_order, color='green')
    plt.scatter(x[1:], reverse_order, label='Forecast', color='green',
                marker='*')
    plt.legend()

    # plt.legend()

    plt.show()


    # plt.subplots_adjust(bottom=0.2)
    # plt.xticks(rotation=25)
    # ax = plt.gca()
    # xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    # ax.xaxis.set_major_formatter(xfmt)
    # plt.plot(t_millis, bl)
    # plt.show()
