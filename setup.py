from distutils.core import setup

setup(
    name='statistics_crowd',
    version='',
    packages=['mobility',
              'mobility.view', 'mobility.entities', 'mobility.googmaps',
              'mobility.plotting', 'mobility.optimizer'],
    url='',
    license='',
    author='marcouras',
    author_email='',
    description=''
)
