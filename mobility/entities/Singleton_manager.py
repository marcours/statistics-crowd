from datetime import datetime

from mobility.entities.Node import Node
from mobility.googmaps.gmaps import move_coordinates
import numpy as np



class Singleton(object):
    def __init__(self, cls):
        self.cls = cls
        self.obj = None

    def __call__(self, *args, **kwargs):
        if self.obj is None: self.obj = self.cls(*args, **kwargs)
        return self.obj


@Singleton
class SingletonManager(object):
    def __init__(self):
        self.time = datetime.now()
        self.nodes = []
        self.fixed_nodes = None
        self.mobile_nodes = 0
        self.event_triggered = False
        self.qoi = 0
        self.beta = 0
        self.alpha = 0
        self.center_point = 0
        self.radius = 0
        self.n_mobile = 0
        self.n_fixed = 0
        # Statistics
        self.arrival_time = []
        self.departure_time = []
        self.dead_time = []
        self.devices_battery = 0
        self.shit_frequencies = 0
        self.life_time = 0
        self.dt = 0
        self.min_b = 0
        self.max_b = 0
        self.speed_limit = 0
        self.energy_limit = 0
        self.sample = 0
        self.dir_name = 0
        self.anim = 0
        self.figu = 0

    def trigger_event(self):
        print ("!!!!!!!!!!!!!!!!! Event Triggered !!!!!!!!!!!!!!!!!!!!!!!!!!!")
        self.event_triggered = True
        from mobility.optimizer.Choosefrequency import frequency_assignment
        for n in self.fixed_nodes:
            n.is_sensing = True
        #frequency_assignment()


    def trigger_reset(self):
        self.event_triggered = False

    def generate_mobile_nodes(self, n_nodes, lat, lon):
        m_b = [Node(lat, lon) for i in range(n_nodes)]
        self.mobile_nodes = m_b
        self.nodes.append(m_b)

    def check_event(self):
        pass

    def get_all_nodes(self):
        return self.nodes[0] + self.nodes[1]

    def get_sensing_nodes(self):

        result = []
        for n in self.get_all_nodes():
            if n.is_sensing:
                result.append(n)

        return result

    def generate_fixed_node(self, n_fixed_nodes):
        # global i, fixed_nodes_coord, fx_nodes
        self.devices_battery = np.empty(shape=(n_fixed_nodes, 1))
        self.shit_frequencies = np.empty(shape=(n_fixed_nodes, 1))
        self.life_time = np.empty(shape=(n_fixed_nodes, 1))


        fixed_nodes_coord = [move_coordinates(self.center_point,
                                              distance=self.radius - self.radius * 0.5 * (
                                                  i / 4),
                                              bearing=i * 45
                                              ) for i in range(n_fixed_nodes)]
        # Creo i nodi della simulazione
        fx_nodes =[Node(pt.latitude, pt.longitude) for pt in fixed_nodes_coord]

        for i,f in enumerate(fx_nodes):
            f.is_fixed = True
            f.is_sensing = True
            self.devices_battery[i] = f.batt_level

        self.nodes.append(fx_nodes)
        self.fixed_nodes = fx_nodes
        return fixed_nodes_coord


if __name__ == '__main__':
    sing_manager = SingletonManager()
    from geopy import Point

    center_lat = 39.228389
    center_lon = 9.109773
    center_point = Point(center_lat, center_lon)
    radius = 50 / 1000.0
    boundary = radius * 1.414
    n_geofence_points = 300
    n_mobile_nodes = 50
    n_fixed_nodes = 3

    sing_manager.qoi = 2 / 60.0  # Hz
    sing_manager.beta = 0.6
    sing_manager.center_point = center_point
    sing_manager.radius = radius
    sing_manager.n_fixed = n_fixed_nodes
    sing_manager.n_mobile = n_mobile_nodes

    sing_manager.generate_mobile_nodes(n_mobile_nodes, center_lat, center_lon)
    sing_manager.generate_fixed_node(10)

    # print("fixed")
    # for node in sing_manager.fixed_nodes:
    #     print (node)
    #
    # print("mobile")
    # for node in sing_manager.mobile_nodes:
    #     print (node)
    #
    # print("all getter")
    # for i,node in enumerate(sing_manager.get_all_nodes()):
    #     print (node)
    #     node.idx = i
    #
    # print("fixed")
    # for node in sing_manager.fixed_nodes:
    #     print (node)
    #     node.idx = 0
    #     print node.is_fixed
    #
    # print("all getter")
    # for i, node in enumerate(sing_manager.get_all_nodes()):
    #     print (node)
    #     print node.is_fixed

    node_list = sing_manager.get_all_nodes()
    are_sensing = [no.is_fixed for no in sing_manager.get_all_nodes()]

    print are_sensing

    for i, n in enumerate(node_list):
        if are_sensing[i]:
            print n
            n.is_sensing = True

    for n in sing_manager.get_all_nodes():
        print n

    print("Query")
    for x in sing_manager.get_sensing_nodes():
        print x