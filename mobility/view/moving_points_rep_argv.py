"""
This short code snippet utilizes the new animation package in
matplotlib 1.1.0; it's the shortest snippet that I know of that can
produce an animated plot in python. I'm still hoping that the
animate package's syntax can be simplified further.
"""
from datetime import datetime

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
# from bokeh.models import pd
from geopy import Point
from geopy.distance import vincenty
import os
import sys
sys.path.append("~/PycharmProjects/statistics_crowd")
import pandas as pd

from mobility.entities.Singleton_manager import SingletonManager
from mobility.googmaps.gmaps import move_coordinates, background_and_pixels
from mobility.googmaps.google_static_maps_api import GoogleStaticMapsAPI
from mobility.googmaps.google_static_maps_api import MAPTYPE
from mobility.googmaps.google_static_maps_api import MAX_SIZE
from mobility.googmaps.google_static_maps_api import SCALE
from mobility.optimizer.Choosefrequency import frequency_assignment
from mobility.plotting.battery_level import plot_battery
from mobility.plotting.event_distribution import plot_events
from mobility.plotting.frequencies import plot_freq
from mobility.plotting.dead_time import plot_dead
from mobility.plotting.life_time import plot_life


def compute_mean_energy():
    sing = SingletonManager()
    bl_l =[]
    for fx_n in sing.fixed_nodes:
        bl_l.append(fx_n.batt_level)

    mean = np.array(bl_l).mean()
    return mean


def simData():
    # this function is called as the argument for
    # the simPoints function. This function contains
    # (or defines) and iterator---a device that computes
    # a value, passes it back to the main program, and then
    # returns to exactly where it left off in the function upon the
    # next call. I believe that one has to use this method to animate
    # a function using the matplotlib animation package.
    global labels

    beta = 0.9
    D_k = 2 * radius * 1000  # m
    F_k = sing_manager.qoi
    beta = sing_manager.beta
    #speed_limit = beta * D_k * F_k  # m/s
    speeds= np.arange(1,10,0.5)

    t_max = 36000.0
    dt = 10

    t = 0
    t_idx = 0

    #for speed_limit in speeds:
    dt = sing_manager.dt
    # speed_limit = sing_manager.speed_limit
    mean_energy = compute_mean_energy()
    e_limit = sing_manager.energy_limit*mean_energy
    print "e_limit: {}".format(e_limit)
    while t < t_max:
        try:
            sing_manager.time = t

            # Aggiornamento delle frequenze
            #frequency_assignment()

            # Sposto le coordinate dal centro di una quantita' variabile
            # per simulare moto rettilineo uniforme
            mobile_nodes_coord = [move_coordinates(center_point,
                                                   distance=
                                                   random_distanced_boundary[n] *
                                                   random_start -
                                                   (t * node.speed)/1000.0,
                                                   bearing=n
                                                   )
                                  for n, node in
                                  enumerate(sing_manager.mobile_nodes)]

            # Genero le coordinate per i nodi in movimento
            d, lat_array, lon_array = lat_lon_data(mobile_nodes_coord)

            # Aggiorno lo stato dei nodi mobili
            update_mobile_nodes(dt, lat_array, lon_array, e_limit, t)

            # Aggiorno lo stato dei nodi fissi
            update_fixed_nodes(dt, t)

            # creo il dataframe per estrarre le coordinate x y
            df = pd.DataFrame(data=d)
            pix = GoogleStaticMapsAPI.to_tile_coordinates(df['latitude'],
                                                          df['longitude'],
                                                          center_lat,
                                                          center_lon,
                                                          zoom, size, SCALE)

            t_idx += 1
            t = t + dt
            yield pix['y_pixel'], pix['x_pixel'], t

        except ZeroDivisionError:
            break


def lat_lon_data(mobile_nodes_coord):

    # trasformo il float in array di numpy per inserire
    # lat/lon nel dataframe
    mobile_lat_list = [mobile_node.latitude
                       for mobile_node in mobile_nodes_coord]
    mobile_lon_list = [mobile_node.longitude
                       for mobile_node in mobile_nodes_coord]

    lat_array = np.array(mobile_lat_list)
    lon_array = np.array(mobile_lon_list)

    # creo il dizionario per il dataframe
    d = {'latitude': lat_array, 'longitude': lon_array, 'colors': 'red'}
    return d, lat_array, lon_array


def update_fixed_nodes(dt, t):
    # Strutture dati per i grafici
    devices_battery = np.empty(shape=(n_fixed_nodes, 1))
    shit_frequencies = np.empty(shape=(n_fixed_nodes, 1))
    life_time = np.empty(shape=(n_fixed_nodes, 1))

    # Aggiornamento dello stato
    for f_idx, fixed_node in enumerate(sing_manager.fixed_nodes):
        devices_battery[f_idx] = fixed_node.batt_level
        shit_frequencies[f_idx] = fixed_node.frequency

        # Stima lifetime
        tau = fixed_node.batt_level / (
        abs(fixed_node.battery_drain * 3.7) + abs(
            fixed_node.frequency) * fixed_node.tx_cost)
        life_time[f_idx] = tau

        # Esecuzione del task
        if fixed_node.nxt_tsk_time < t and fixed_node.is_sensing:
            fixed_node.execute_task()
            fixed_node.batt_level -= fixed_node.tx_cost
            if fixed_node.frequency is not 0:
                fixed_node.nxt_tsk_time = t + 1.0 / fixed_node.frequency
            print("next {}".format(fixed_node.nxt_tsk_time))

        # Decadimento naturale della batteria
        delta_b = fixed_node.battery_drain * (dt / 3600.0)
        fixed_node.batt_level += delta_b

        # Istante di aggiornamento
        fixed_node.time = t
        if fixed_node.is_dead:
            fixed_node.batt_level = 0

        # Memorizzazione dell'stante di spegnimento
        if (
            fixed_node.batt_level < 0) and fixed_node.is_sensing and not fixed_node.is_dead:
            sing_manager.dead_time.append(t)
            print("135_mov")
            sing_manager.trigger_event()
            fixed_node.batt_level = 0
            fixed_node.is_dead = True
            # print (fixed_node)

    # Raccolta dati per i plot
    sing_manager.devices_battery = np.concatenate(
        (sing_manager.devices_battery,
         devices_battery), axis=1)
    sing_manager.shit_frequencies = np.concatenate(
        (sing_manager.shit_frequencies, shit_frequencies), axis=1)
    sing_manager.life_time = np.concatenate(
        (sing_manager.life_time, life_time), axis=1)

def update_mobile_nodes(dt, lat_array, lon_array, energy_limit, t):

    for idx, mobile_node in enumerate(sing_manager.mobile_nodes):
        mobile_node.time = t

        # Sposto le coordinate
        mobile_node.latitude = lat_array[idx]
        mobile_node.longitude = lon_array[idx]

        # Decadimento naturale della batteria
        delta_batt = mobile_node.battery_drain * (dt / 3600.0)
        mobile_node.batt_level += delta_batt

        # Verifica della posizione interna o esterna alla geofence
        test_point = Point(mobile_node.latitude, mobile_node.longitude)
        distance = vincenty(test_point, center_point).kilometers

        # trigger evento di ingresso/uscita
        if distance < radius:
            #print ("node energy: {} | energy limit {}".format(mobile_node.batt_level,energy_limit))
            # Filtro alpha sulla carica residua
            if mobile_node.batt_level > energy_limit:

                mobile_node.execute_task()
                mobile_node.batt_level -= mobile_node.tx_cost

                # Verifico se primo ingresso
                # todo verifica che abbia carica residua
                if not mobile_node.is_sensing:
                    print("103_mov")
                    frequency_assignment()
                    sing_manager.trigger_event()
                    sing_manager.arrival_time.append(t)
                mobile_node.is_sensing = True
        else:
            # Trigger di uscita
            if mobile_node.is_sensing:
                frequency_assignment()
                print ("109_mov")
                sing_manager.trigger_event()
                sing_manager.departure_time.append(t)
                mobile_node.is_sensing = False


# def update_mobile_nodes(dt, lat_array, lon_array, speed_limit, t):
#
#     for idx, mobile_node in enumerate(sing_manager.mobile_nodes):
#         mobile_node.time = t
#
#         # Sposto le coordinate
#         mobile_node.latitude = lat_array[idx]
#         mobile_node.longitude = lon_array[idx]
#
#         # Decadimento naturale della batteria
#         delta_batt = mobile_node.battery_drain * (dt / 3600.0)
#         mobile_node.batt_level += delta_batt
#
#         # Verifica della posizione interna o esterna alla geofence
#         test_point = Point(mobile_node.latitude, mobile_node.longitude)
#         distance = vincenty(test_point, center_point).kilometers
#
#         # trigger evento di ingresso/uscita
#         if distance < radius:
#             print ("node speed: {} | speed_limit {}".format(mobile_node.speed,
#                                                                 speed_limit))
#             # Filtro beta sulla velocita
#             if mobile_node.speed < speed_limit:
#
#                 mobile_node.execute_task()
#                 mobile_node.batt_level -= mobile_node.tx_cost
#
#                 # Verifico se primo ingresso
#                 # todo verifica che abbia carica residua
#                 if not mobile_node.is_sensing:
#                     print("103_mov")
#                     frequency_assignment()
#                     sing_manager.trigger_event()
#                     sing_manager.arrival_time.append(t)
#                 mobile_node.is_sensing = True
#         else:
#             # Trigger di uscita
#             if mobile_node.is_sensing:
#                 frequency_assignment()
#                 print ("109_mov")
#                 sing_manager.trigger_event()
#                 sing_manager.departure_time.append(t)
#                 mobile_node.is_sensing = False


def simPoints(simData):
    """
    Vengono calcolate j traiettorie sulla base delle coordinate fornite
    da simData
    :param simData:
    :return:
    """
    y, x, t = simData[0], simData[1], simData[2]

    # Dati per animazione
    time_text.set_text(time_template % (t))
    line.set_data(x, y)
    return line, time_text


def plot_fixed_nodes():
    global fig, labels
    fig = plt.figure(figsize=(10, 10))
    plt.imshow(np.array(img))  # Background map
    plt.scatter(  # Scatter plot for geofence
        pixels['x_pixel'],
        pixels['y_pixel'],
        c='red',
        s=width / 40,
        linewidth=0.5,
        alpha=0.5,
    )
    plt.scatter(  # Scatter plot for fixed nodes
        pixels['x_pixel'][-n_fixed_nodes - 1:-2],
        pixels['y_pixel'][-n_fixed_nodes - 1:-2],
        c='green',
        s=width / 40,
        linewidth=0.5,
        alpha=1,
    )
    labels = ['batt: {0:.1f}'.format(m_node.batt_level)
              for m_node in sing_manager.fixed_nodes]
    for label, x, y in zip(labels, pixels['x_pixel'][-n_fixed_nodes - 1:-2],
                           pixels['y_pixel'][-n_fixed_nodes - 1:-2]):
        plt.annotate(
            label,
            xy=(x, y), xytext=(-15, 15),
            textcoords='offset points', ha='right', va='bottom',
            bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
            arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
    plt.gca().invert_yaxis()  # Origin of map is upper left
    plt.axis([0, width, width, 0])  # Remove margin
    plt.axis('off')
    plt.tight_layout()


def plot_animated_mobile_nodes():
    global line, time_template, time_text
    sn = SingletonManager()
    ax = fig.add_subplot(111)
    # I'm still unfamiliar with the following line of code:
    line, = ax.plot([], [], 'bo', ms=10)
    # ax.set_ylim(39, 39.50)
    # ax.set_xlim(9, 9.50)
    ##
    time_template = 'Time = %.1f s'  # prints running simulation time
    time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
    # Now call the animation package: (simData is the user function
    # serving as the argument for simPoints):
    ani = animation.FuncAnimation(fig, simPoints, simData, blit=False,
                                  interval=10, repeat=False)

    plt.ion()
    plt.show()

def setting_background():
    """
    Scaricamento della mappa con un dato valore di zoom, ricavato dalla lista
    di lat_lon fornite nel dataframe
    :return:
    """
    global img, pixels, zoom, size
    moved_points = [move_coordinates(center_point,
                                     distance=boundary,
                                     bearing=(i + 1) * 1.2
                                     ) for i in range(n_geofence_points)]
    lat_list = [p.latitude for p in moved_points]
    lon_list = [p.longitude for p in moved_points]
    t_list = np.arange(0, 10, 0.05)
    square = [move_coordinates(center_point,
                               distance=boundary * 1.5,
                               bearing=i * 90
                               ) for i in range(4)]
    for s in square:
        lat_list.append(s.latitude)
        lon_list.append(s.longitude)
    fixed_nodes_coord = sing_manager.generate_fixed_node(n_fixed_nodes)
    frequency_assignment()
    for idx, node_f in enumerate(sing_manager.fixed_nodes):
        sing_manager.shit_frequencies[idx] = node_f.frequency
        tau = node_f.batt_level / (
        node_f.battery_drain + node_f.frequency * node_f.tx_cost)
        sing_manager.life_time[idx] = abs(tau)
    for f in fixed_nodes_coord:
        lat_list.append(f.latitude)
        lon_list.append(f.longitude)
    lat_list.append(center_lat)
    lon_list.append(center_lon)
    lat_list = np.array(lat_list)
    lon_list = np.array(lon_list)
    d = {'latitude': lat_list, 'longitude': lon_list, 'colors': 'red'}
    df = pd.DataFrame(data=d)
    r = background_and_pixels(df['latitude'], df['longitude'], MAX_SIZE,
                              maptype)
    img, pixels, zoom, size = r[0], r[1], r[2], r[3]


def setup_parameters():
    global center_lat, center_lon, center_point, radius, boundary, n_geofence_points, n_fixed_nodes, maptype, width, random_distanced_boundary, sing_manager, random_start
    day = datetime.now().day
    hour = datetime.now().hour
    min = datetime.now().minute
    sec = datetime.now().second
    center_lat = 39.228389
    center_lon = 9.109773
    center_point = Point(center_lat, center_lon)
    radius = 50 / 1000.0
    boundary = radius * 1.414
    n_geofence_points = 300
    n_mobile_nodes = int(sys.argv[1])
    n_fixed_nodes = int(sys.argv[2])
    maptype = MAPTYPE
    width = SCALE * MAX_SIZE
    # Una serie di coefficienti di accrocchio
    spread_boundary = 1
    min_v = float(sys.argv[3])
    max_v = int(sys.argv[4])
    random_distanced_boundary = np.ones(n_mobile_nodes)
    # Istanza del Singleton
    sing_manager = SingletonManager()
    sample = int(sys.argv[5])
    #speed_limit = float(sys.argv[6])
    energy_limit = float(sys.argv[6])
    dt = float(sys.argv[7])

    sing_manager.qoi = sample / 60.0  # campioni al minuto
    sing_manager.generate_mobile_nodes(n_mobile_nodes, center_lat, center_lon)
    sing_manager.beta = 0.6
    sing_manager.center_point = center_point
    sing_manager.radius = radius
    sing_manager.n_fixed = n_fixed_nodes
    sing_manager.n_mobile = n_mobile_nodes
    sing_manager.min_b = min_v
    sing_manager.max_b = max_v
    sing_manager.sample = sample
    sing_manager.dt = dt
    #sing_manager.speed_limit = speed_limit
    sing_manager.energy_limit = energy_limit
    for id in range(n_mobile_nodes):
        # np.random.seed(id)
        random_start = (max_v - min_v) * np.random.random_sample() + min_v
        random_distanced_boundary[
            id] = boundary * spread_boundary * random_start


if __name__ == '__main__':
    # -------------------------- Initial Setup ------------------------------ #

    ##
    # set up figure for plotting:
    ##

    setup_parameters()
    # ----------------------------------------------------------------------- #

    # ------------- GENERAZIONE PUNTI SU CIRCONFERENZA e QUADRATO ----------- #
    setting_background()

    # coordinate del centro
    x_c = np.array(pixels['x_pixel'])[-1]
    y_c = np.array(pixels['y_pixel'])[-1]
    # ----------------------------------------------------------------------- #

    # -------------------- PUNTI SULLA GEOFENCE ----------------------------- #
    plot_fixed_nodes()
    # ----------------------------------------------------------------------- #

    # -------------------- PUNTI CHE SI MUOVONO ----------------------------- #


    plot_animated_mobile_nodes()



    # ----------------------------------------------------------------------- #
    sinnn = SingletonManager()
    # Mobile node | fixed | distance interval | qoi | speed | dt
    simulation_name = "m_{}_fx_{}_min_b_{}_max_b_{}_qoi_{}_s_{}_dt_{}".format(
        sinnn.n_mobile,
        sinnn.n_fixed,
        sinnn.min_b,
        sinnn.max_b,
        sinnn.sample,
        sinnn.energy_limit,
        sinnn.dt
    )

    print simulation_name
    sinnn.dir_name = simulation_name

    if not os.path.exists("../plotting/output_plots/{}".format(simulation_name)):
        os.mkdir("../plotting/output_plots/{}".format(simulation_name))

    fig_size = (15, 10)
    plt.figure(figsize=fig_size)
    plot_events()
    plt.savefig("../plotting/output_plots/{}/events".format(simulation_name))


    plt.figure(figsize=fig_size)
    plot_dead()
    plt.savefig("../plotting/output_plots/{}/dead".format(simulation_name))


    plt.figure(figsize=fig_size)
    plot_battery()
    plt.savefig("../plotting/output_plots/{}/battery".format(simulation_name))


    plt.figure(figsize=fig_size)
    plot_freq()
    plt.savefig("../plotting/output_plots/{}/freq".format(simulation_name))


    plt.figure(figsize=fig_size)
    plot_life()
    plt.savefig("../plotting/output_plots/{}/life".format(simulation_name))




    plt.figure(figsize=(45, 30))
    plt.subplot(3,2,1)
    plot_events()

    plt.subplot(3,2,2)
    plot_dead()

    plt.subplot(3,2,3)
    plot_battery()

    plt.subplot(3,2,4)
    plot_freq()

    plt.subplot(3,2,5)
    plot_life()
    plt.savefig("../plotting/output_plots/{}/total".format(simulation_name))


