import datetime as dt

import matplotlib.pyplot as plt
import numpy as np

# with open('raimondo.json') as data_file:
#     data = json.load(data_file)


t5_1 = {
    "timestamp": "Tue Jun 27 11:22:39 UTC 2017",
    "name": "BATTERY-LEVEL",
    "value": "0.8799999952316284",
    "type": "number",
    "valueMah": "2552.0"
}


t5_2 = {
            "timestamp": "Tue Jun 27 11:17:37 UTC 2017",
            "name": "BATTERY-LEVEL",
            "value": "0.8899999856948853",
            "type": "number",
            "valueMah": "2581.0"
        }


t1_1 = {
            "timestamp": "Tue Jun 27 11:00:54 UTC 2017",
            "name": "BATTERY-LEVEL",
            "value": "0.8999999761581421",
            "type": "number",
            "valueMah": "2610.0"
        }

t1_2 = {
            "timestamp": "Tue Jun 27 10:14:13 UTC 2017",
            "name": "BATTERY-LEVEL",
            "value": "0.9200000166893005",
            "type": "number",
            "valueMah": "2668.0"\
        }


# Parse the time strings
t1 = dt.datetime.strptime(t1_1['timestamp'], '%a %b %d %H:%M:%S %Z %Y')
t2 = dt.datetime.strptime(t1_2['timestamp'], '%a %b %d %H:%M:%S %Z %Y')

# Do the math, the result is a timedelta object
print t1
print t2
delta = (t1 - t2)

print(delta).seconds


t5_millis = dt.datetime


b_brain_1 = 124
b_drain_5 = 58


N = 3
fig, ax = plt.subplots()
ind = np.arange(1, N+1, 1)  # the x locations for the groups
print ind
width = 0.15       # the width of the bars


position_means = (172, 138, 117)
position_std = (2, 3, 4)
rects1 = ax.bar(ind, position_means, width, color='r' )#, yerr=position_std)

brightness_means = (132, 73, 44)
brightness_std = (3, 5, 2)
rects2 = ax.bar(ind + width, brightness_means, width, color='y')#, yerr=brightness_std)


no_sampling_means = (117, 58, 36)
other_std = (2, 3, 2)
rects3 = ax.bar(ind + 2 * width, no_sampling_means, width, color='b')#, yerr=other_std)

# add some text for labels, title and axes ticks
ax.set_ylabel('Battery Drain (mA)',fontsize=20)
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('1', '0.5', '0.1'),fontsize=18)
ax.set_xlabel('Schedule (samples/min)',fontsize=20)

ax.legend((rects1[0], rects2[0],rects3[0]), ('Position', 'Brightness','No Sampling'))


def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()

