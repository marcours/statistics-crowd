from geopy import Point
from geopy.distance import vincenty

from mobility.entities.Node import Node


def move_coordinates(point, distance, bearing):
    vince = vincenty()
    new_point = vincenty.destination(vince, point=point,
                                     bearing=bearing, distance=distance)

    return new_point


class Geofence(object):
    def __init__(self, radius, qoi, center_lat=39.228389, center_lon=9.109773):
        self.center_lat = center_lat
        self.center_lon = center_lon
        self.center_point = Point(center_lat, center_lon)
        self.radius = radius
        self.qoi = qoi
        self.connected_nodes = []
        self.mobile_nodes = []

    def generate_internal_nodes(self, n_nodes):
        #Node(1,self.center_lat, self.center_lon)
        self.connected_nodes = [Node(
                                     latitude=self.center_lat,
                                     longitude=self.center_lon
                                     ) for i in range(n_nodes)]
        for n in self.connected_nodes:
            n.is_sensing = True

    def generate_external_nodes(self, n_nodes):
        boundary = self.radius * 1.414

        moved_points = [move_coordinates(self.center_point,
                                         distance=boundary,
                                         bearing=(i+1) * 10
                                         ) for i in range(n_nodes)]
        self.mobile_nodes = [Node(
                                  p.latitude,
                                  p.longitude
                                  ) for i, p in enumerate(moved_points)]


if __name__ == '__main__':
    google_api_key = "AIzaSyBlecPxd1S9sM8Fk3iEM6AGyw5wNNXEIac"

    g = Geofence(radius=100, qoi=20)
    g.generate_internal_nodes(10)
    g.generate_external_nodes(20)

    for n in g.mobile_nodes:
        print n

    for m in g.connected_nodes:
        print m

