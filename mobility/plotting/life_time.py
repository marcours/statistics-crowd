import numpy as np
from mobility.entities.Singleton_manager import SingletonManager
from matplotlib import pyplot as plt


def plot_life():
    singl_man = SingletonManager()

    x_axis = np.arange(singl_man.life_time.shape[1])

    y_axis = singl_man.life_time

    plt.ylim(0,2)
    for i, lt in enumerate(y_axis):
        plt.plot(x_axis, lt)

    plt.grid()
    plt.title("Lifetime")
