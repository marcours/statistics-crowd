import urllib
import json
import requests


def get_history_json(vo_id, h_start, m_start, h_stop, m_stop, day, month=06,
                     limit=200):
    """

    :param vo_id: SVO id
    :param h_start:
    :param m_start:
    :param h_stop:
    :param m_stop:
    :param day:
    :param month:
    :param limit: limite di entries da prendere dal datastore
    :return: JSON Array

    {u'vo_id': u'17', u'schedule': u'300', u'tx-cost': None,
    u'radio-technoloy': u'WIFI', u'battery-drain': u'-210.916885712',
    u'battery-level': u'1283.68937893', u'radio-rssi': u'-61.0291110495',
    u'timestamp': u'2017-06-25 18:28:01', u'position': u'39.22824324,9.109696447'}
    """

    url = "http://svoemulator2.appspot.com/getHistory"

    limit = "{}".format(limit)
    vo_id = "{}".format(vo_id)
    start_time = "2017-{}-{} {}:{}:00".format(month, day, h_start, m_start)
    stop_time = "2017-{}-{} {}:{}:00".format(month, day, h_stop, m_stop)

    payload = urllib.urlencode(
        {"vo_id": vo_id, "limit": limit, "start-time": start_time,
         "stop-time": stop_time})
    headers = {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",

    }

    response = requests.request("POST", url, data=payload, headers=headers)

    tx = response.text

    data = json.loads(tx)

    return data
