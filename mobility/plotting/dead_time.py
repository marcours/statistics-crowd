import json

import numpy as np
from mobility.entities.Singleton_manager import SingletonManager
from matplotlib import pyplot as plt


def plot_dead():
    singl_man = SingletonManager()

    x_axis = singl_man.dead_time
    c = 0
    y_axis = np.zeros(x_axis.__len__())


    for i in range(x_axis.__len__()):
        c += 1
        y_axis[i] = c

    tau = np.mean(x_axis)
    print ("dead_time | tau: {}".format(tau))


    fname = "../plotting/output_plots/{}/tau.json".format(singl_man.dir_name)
    f2name = "../plotting/output_plots/{}/dead.json".format(singl_man.dir_name)

    dead_json = {"dead":x_axis}
    tau_json = {"tau":tau}

    with open(f2name, 'w') as f:
        f.write(json.dumps(dead_json, indent=4, separators=(',', ': ')))
    with open(fname, 'w') as f:
        f.write(json.dumps(tau_json, indent=4, separators=(',', ': ')))

    plt.title("Number of dead Devices")
    plt.plot(x_axis,y_axis, 'o', c='g')


