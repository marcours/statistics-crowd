import urllib
import json
from pprint import pprint

import requests


def get_svo_history_json(vo_id, start_time = None, stop_time=None, sensor="BATTERY-LEVEL", limit=400):
    """
    :param vo_id: SVO id
    :param h_start:
    :param m_start:
    :param h_stop:
    :param m_stop:
    :param day:
    :param month:
    :param year:
    :param limit: limite di entries da prendere dal datastore
    :return: JSON Array

    {u'vo_id': u'17', u'schedule': u'300', u'tx-cost': None,
    u'radio-technoloy': u'WIFI', u'battery-drain': u'-210.916885712',
    u'battery-level': u'1283.68937893', u'radio-rssi': u'-61.0291110495',
    u'timestamp': u'2017-06-25 18:28:01', u'position': u'39.22824324,9.109696447'}
    """

    url = "http://{}.appspot.com/getSensor?all".format(vo_id)

    limit = "{}".format(limit)

    payload = {"limit": limit, "sensorName": sensor}

    if (stop_time is not None) or (start_time is not None):
        payload = urllib.urlencode(
            {"limit": limit, "startTime": start_time,
             "endTime": stop_time, "sensorName": sensor})

    headers = {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",

    }

    response = requests.request("POST", url, data=payload, headers=headers)

    tx = response.text

    data = json.loads(tx)

    return data

if __name__ == '__main__':

    limit = 600
    h_start = 0
    m_start = 0
    h_stop = 0
    m_stop = 00
    day = 22
    month = 3
    vo = "lysis-77"
    sensor = "BATTERY-LEVEL"
    data = get_svo_history_json(vo_id=vo,sensor=sensor, h_start=h_start, m_start=m_start,
                                h_stop=h_stop, m_stop=m_stop, day=day,
                                month=month)

    pprint (data)