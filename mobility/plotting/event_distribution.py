import numpy as np
from mobility.entities.Singleton_manager import SingletonManager
from matplotlib import pyplot as plt


def plot_events():

    singl_man = SingletonManager()

    y_axis_arrival = np.ones(singl_man.arrival_time.__len__())
    x_axis_arrival = singl_man.arrival_time
    y_axis_departure = -np.ones(singl_man.departure_time.__len__())
    x_axis_departure = singl_man.departure_time

    plt.plot(x_axis_arrival,y_axis_arrival,'|', c='g')
    plt.plot(x_axis_departure, y_axis_departure, '|', c='r')
    plt.bar(x_axis_arrival,y_axis_arrival,color='b',width=5)
    plt.bar(x_axis_departure,y_axis_departure,color='r',width=5)
    plt.title("Event Distribution")