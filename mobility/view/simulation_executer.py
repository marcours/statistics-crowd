import os
import numpy as np
import itertools


# m_nodes = [300,150]
# f_nodes = [15]
# min_b = [0.5, 1,2]
# max_b = [15,20]
# sample = [10,15,20]
# speed_limit = [1,2,3,4,5,6,7,8,9,10]
# energy_limit = [0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
# dt = [1,5,10]

m_nodes = [300]
f_nodes = [15]
min_b = [2.0]
max_b = [20]
sample = [15]
speed_limit = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]
energy_limit = [0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
dt = [5.0]


j=0
for elem in itertools.product(m_nodes,f_nodes,min_b,max_b,sample,speed_limit,dt,energy_limit):
    j +=1

print "Totale simulazioni {}".format(j)

i = 0
for elem in itertools.product(m_nodes,f_nodes,min_b,max_b,sample,speed_limit,dt,energy_limit):
    i += 1
    simulation_name = "m_{}_fx_{}_min_b_{}_max_b_{}_qoi_{}_s_{}_e_{}_dt_{}".format(
        elem[0],
        elem[1],
        elem[2],
        elem[3],
        elem[4],
        elem[5],
        elem[7],
        elem[6]
    )
    command = "python /Users/marcouras/PycharmProjects/statistics_crowd/mobility/view/moving_points_rep_argv_alpha_beta.py {} {} {} {} {} {} {} {}".format(*elem)
    #print (command)
    print "Simulazione :{}".format(i)
    if not os.path.exists("../plotting/output_plots/a_b/{}".format(simulation_name)):
        os.system(command)
        #path = "../plotting/output_plots/a_b/{}".format(simulation_name)
        #print path
    else:
        print ("simulation skipped")

print i

# command = "python /home/ubimate/PycharmProjects/statistics-crowd/mobility/view/moving_points_rep_argv.py 300 15 0.5 15 1 1 0.5"
# os.system(command)
# os.system
# i=0
# os.system (
# "python /home/ubimate/PycharmProjects/statistics-crowd/mobility/view/moving_points_rep_argv.py {} {} {} {} {} {} {}".format(
#     m_nodes[i],
#     f_nodes[i],
#     min_b[i],
#     max_b[i],
#     sample[i],
#     speed_limit[i],
#     dt[i]))
