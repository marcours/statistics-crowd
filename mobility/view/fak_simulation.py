import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from bokeh.models import pd
from geopy import Point
from geopy.distance import vincenty
from scipy import signal

from mobility.entities.Node import Node
from mobility.entities.Singleton_manager import SingletonManager
from mobility.googmaps.gmaps import move_coordinates, background_and_pixels, \
    move_coordinates_dist

from mobility.googmaps.google_static_maps_api import GoogleStaticMapsAPI
from mobility.googmaps.google_static_maps_api import MAPTYPE
from mobility.googmaps.google_static_maps_api import MAX_SIZE
from mobility.googmaps.google_static_maps_api import SCALE

import numpy as np
from scipy.spatial.distance import pdist, squareform

import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

# -------------------------- Initial Settings ------------------------------- #

center_lat = 39.228389
center_lon = 9.109773
center_point = Point(center_lat, center_lon)
radius = 50 / 1000.0
boundary = radius * 1.414
n_geofence_points = 300
n_mobile_nodes = 4
n_fixed_nodes = 9
maptype = MAPTYPE
width = SCALE * MAX_SIZE


# --------------------------------------------------------------------------- #


class NodeBox:
    """Orbits class

    init_state is an [N x 4] array, where N is the number of particles:
       [[x1, y1, vx1, vy1],
        [x2, y2, vx2, vy2],
        ...               ]

    bounds is the size of the box: [xmin, xmax, ymin, ymax]
    """

    def __init__(self,
                 init_state=[[1, 0, 0, -1],
                             [-0.5, 0.5, 0.5, 0.5],
                             [-0.5, -0.5, -0.5, 0.5]],
                 bounds=[-2, 2, -2, 2],
                 size=0.04,
                 M=0.15):
        self.init_state = np.asarray(init_state, dtype=float)
        self.M = M * np.ones(self.init_state.shape[0])
        self.size = size
        self.state = self.init_state.copy()
        self.time_elapsed = 0
        self.bounds = bounds

    def step(self, dt):
        """step once by dt seconds"""
        self.time_elapsed += dt

        # update positions
        self.state[:, :2] += dt * self.state[:, 2:]


# ---------------------- Setup Initial State -------------------------------- #
np.random.seed(0)
init_state = np.random.random((50, 4))
init_state[:, :2] *= 1200
init_state[:, 2:] -= 0.5
init_state[:, 2:] *= 100

box = NodeBox(init_state, size=0.04)
dt = 1. / 30  # 30fps

# --------------------------------------------------------------------------- #


# ------------------------ Setup Animation ---------------------------------- #

fig = plt.figure()
square = [move_coordinates(center_point,
                           distance=boundary * 1.5,
                           bearing=i * 90
                           ) for i in range(4)]

lat_list = [s.latitude for s in square]
lon_list = [s.longitude for s in square]

d = {'latitude': lat_list, 'longitude': lon_list, 'colors': 'red'}
df = pd.DataFrame(data=d)
r = background_and_pixels(df['latitude'], df['longitude'], MAX_SIZE,
                          maptype)
img, pixels, zoom, size = r[0], r[1], r[2], r[3]

print(pixels)

ax = fig.add_subplot(111)
plt.gca().invert_yaxis()  # Origin of map is upper left
plt.axis([0, width, width, 0])  # Remove margin
plt.axis('off')
plt.tight_layout()
plt.imshow(np.array(img))  # Background map

# Genero un segnale triangolare per simulare un movimento periodico
triangle = abs(signal.sawtooth(
    2 * np.pi * t * (distanced_boundary / speed_factor), 0.5))
triangle_2 = abs(signal.sawtooth(
    2 * np.pi * (t + dt) * (distanced_boundary / speed_factor), 0.5))

# ---------------------------sim data------------------------------------------ #

# Calcolo la posizione successiva per avere lo spostamento in dt
# e quindi la velocit
point_n_dist2 = move_coordinates_dist(center_point,
                                      distance=(
                                          0.05),
                                      # bearing=n_iter*1000 ininfluente
                                      bearing=np.arctan(t)
                                      )

# Le coordinate del primo punto e la distanza dal centro
points = point_n_dist[0]

# Distanza dal centro del primo e del secondo punto
dist = point_n_dist[1]
dist2 = point_n_dist2[1]


# ############# LISTE di coordinate x y per i punti mobili ############## #
# x_points = []
# y_points = []

# creo delle traiettorie sulla base di quella creata su SimData
# for j, alpha_j in enumerate(np.arange(0, 360, 360.0 / n_mobile_nodes)):
#     ro = np.sqrt((x - x_c) ** 2 + (y - y_c) ** 2)
#     theta_rad = 2 * np.arctan((y - y_c) / (ro + (x - x_c)))
#     theta_deg = np.math.degrees(theta_rad)
#
#     # RO * COS(THETA) + X_C
#     xj_trajectory = np.sign(x - x_c) * \
#                     ro * (1) * \
#                     np.cos(alpha_j * (np.pi / 180.0)) + x_c
#
#     # RO * SIN(THETA) + Y_C
#     yj_trajectory = np.sign(y - y_c) * \
#                     ro * (1) * \
#                     np.sin(alpha_j * (np.pi / 180.0)) + y_c
#
#     x_points.append(xj_trajectory)
#     y_points.append(yj_trajectory)
# # ################### #################### ############################## #
