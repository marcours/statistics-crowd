import json
from datetime import datetime

from geopy import Point
from geopy.distance import vincenty
import numpy as np
import os


class Node(Point):
    instances = 0

    def __init__(self, latitude, longitude):
        super(Node, self).__new__(Point, latitude=latitude,
                                  longitude=longitude, altitude=0)
        self.idx = datetime.now().microsecond
        Node.instances += 1
        # self.latitude = latitude
        # self.longitude = longitude
        self.frequency = 0
        self.is_sensing = False
        self.generate_battery()
        self.generate_tx()
        self.generate_drain()
        self.generate_speed()  # m/s
        self.time = 0.0
        self.is_fixed = False
        self.nxt_tsk_time = 0
        self.is_dead = False

    def __str__(self):
        return '"id": {} | "Lat": {:08.6f} | "Lon": {:08.6f} | "tx": {:6.5f} ' \
               '| "drain": {:9.2f} | "level": {:.8f} | "isSensing": {} ' \
               '| "batt_start": {:.8f} | "frequency": {:.9f} ' \
               '| "speed": {:.5f} | "time": {} | "isFixed": {} ' .format(
            self.idx,
            self.latitude,
            self.longitude,
            self.tx_cost,
            self.battery_drain,
            self.batt_level,
            self.is_sensing,
            self.batt_start,
            self.frequency,
            self.speed*1000,
            self.time,
            self.is_fixed,


        )

    def distance(self, n):
        me = (self.latitude, self.longitude)
        test_point = (n.latitude, n.longitude)
        # d2 = (self.x - p.x) ** 2 + (self.y - p.y) ** 2
        return vincenty(test_point, me).meters

    def set_frequency(self, f):
        print("-------------------- Frequency Setted ------------------------")

        self.batt_level -= self.tx_cost
        self.frequency = f


    def execute_task(self):
        # print("--------------------- Task executed --------------------------")
        #self.batt_level -= self.tx_cost
        pass

    def generate_battery(self):
        # print Node.instances
        np.random.seed(Node.instances*2)
        max_v =  1600
        min_v =  1500
        self.batt_level = ((max_v - min_v) * np.random.random_sample() + min_v)
        self.batt_start = self.batt_level

    def generate_drain(self):
        np.random.seed(Node.instances*3)
        max_v = 250
        min_v = 200
        self.battery_drain = (- ((max_v - min_v) * np.random.random_sample() + min_v))

    def generate_tx(self):
        np.random.seed(Node.instances*4)
        max_v = 50.036
        min_v = 30.009
        self.tx_cost = (max_v - min_v) * np.random.random_sample() + min_v

    def generate_speed(self):

        np.random.seed(Node.instances)

        max_v = 3  # m/s
        min_v = 1
        self.speed = (max_v - min_v) * np.random.exponential(1) + min_v

        #print (self.speed)
        # max_v = 80/1000.0  # m/s
        # min_v = 40/1000.0
        # self.speed = (max_v - min_v) * np.random.random_sample() + min_v

    def refresh_parameters(self):
        self.generate_battery()
        self.generate_tx()
        self.generate_drain()
        #self.generate_speed()


if __name__ == '__main__':
    n1 = Node(0, 2.2)
    n2 = Node(1, 3.3)

    print n2.is_fixed


    # print n1.distance(n2)
    # print "long {}".format(n1.longitude)
    # print "lat {}".format(n1.latitude)

    n2.refresh_parameters()
    print n2
    s = json.dumps(n2.__dict__)

    print s
    l = json.loads(s)
    print l

    d = dict(l)
    print d


    day = datetime.now().day
    hour = datetime.now().hour
    min = datetime.now().minute
    sec = datetime.now().second
    fname = "data/run_{}_{}:{}:{}.json".format(day,hour,min,sec)
    for i in range(5):
        if os.path.exists(fname):
            with open(fname, 'r+') as f:
                oldjson = json.load(f)
        else:
            oldjson = []

        oldjson.append(n2.__dict__)
        with open(fname, 'w') as f:
            f.write(json.dumps(oldjson, indent=4, separators=(',', ': ')))
        n2.refresh_parameters()
        # with open(fname, 'w') as outfile:
        #     if np.os.path.exists(fname):
        #         with open(fname, 'r+') as f:
        #             playlist = json.load(f)
        #             # update json here
        #             f.seek(0)
        #             f.truncate()
        #             json.dump(playlist, f)
        #     json.dump(n2.__dict__, outfile)
        # n2.refresh_parameters()

