# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import unicode_literals

import numpy as np
import pandas as pd
import scipy.ndimage as ndi
from geopy import Point
from geopy.distance import vincenty
from matplotlib import pyplot as plt

from google_static_maps_api import GoogleStaticMapsAPI
from google_static_maps_api import MAPTYPE
from google_static_maps_api import MAX_SIZE
from google_static_maps_api import SCALE

BLANK_THRESH = 2 * 1e-3  # Value below which point in a heatmap should be blank


def background_and_pixels(latitudes, longitudes, size, maptype):
    """Queries the proper background map and translate geo coordinated into pixel locations on this map.

    :param pandas.Series latitudes: series of sample latitudes
    :param pandas.Series longitudes: series of sample longitudes
    :param int size: target size of the map, in pixels
    :param string maptype: type of maps, see GoogleStaticMapsAPI docs for more info

    :return: map and pixels
    :rtype: (PIL.Image, pandas.DataFrame)
    """
    # From lat/long to pixels, zoom and position in the tile
    center_lat = (latitudes.max() + latitudes.min()) / 2
    center_long = (longitudes.max() + longitudes.min()) / 2
    zoom = GoogleStaticMapsAPI.get_zoom(latitudes, longitudes, size, SCALE)
    pixels = GoogleStaticMapsAPI.to_tile_coordinates(latitudes, longitudes,
                                                     center_lat, center_long,
                                                     zoom, size, SCALE)
    # Google Map
    img = GoogleStaticMapsAPI.map(
        center=(center_lat, center_long),
        zoom=zoom,
        scale=SCALE,
        size=(size, size),
        maptype=maptype,
    )
    return img, pixels, zoom, size


def scatter(latitudes, longitudes, colors=None, maptype=MAPTYPE):
    """Scatter plot over a map. Can be used to visualize clusters by providing the marker colors.

    :param pandas.Series latitudes: series of sample latitudes
    :param pandas.Series longitudes: series of sample longitudes
    :param pandas.Series colors: marker colors, as integers
    :param string maptype: type of maps, see GoogleStaticMapsAPI docs for more info

    :return: None
    """
    width = SCALE * MAX_SIZE
    colors = pd.Series(0, index=latitudes.index) if colors is None else colors
    img, pixels = background_and_pixels(latitudes, longitudes, MAX_SIZE,
                                        maptype)
    plt.figure(figsize=(10, 10))
    plt.imshow(np.array(img))  # Background map
    plt.scatter(  # Scatter plot
        pixels['x_pixel'],
        pixels['y_pixel'],
        c=colors,
        s=width / 40,
        linewidth=0,
        alpha=0.5,
    )
    plt.gca().invert_yaxis()  # Origin of map is upper left
    plt.axis([0, width, width, 0])  # Remove margin
    plt.axis('off')
    plt.tight_layout()
    plt.show()


def grid_density_gaussian_filter(data, size, resolution=None,
                                 smoothing_window=None):
    """Smoothing grid values with a Gaussian filter.

    :param [(float, float, float)] data: list of 3-dimensional grid coordinates
    :param int size: grid size
    :param int resolution: desired grid resolution
    :param int smoothing_window: size of the gaussian kernels for smoothing

    :return: smoothed grid values
    :rtype: numpy.ndarray
    """
    resolution = resolution if resolution else size
    k = (resolution - 1) / size
    w = smoothing_window if smoothing_window else int(
        0.01 * resolution)  # Heuristic
    imgw = (resolution + 2 * w)
    img = np.zeros((imgw, imgw))
    for x, y, z in data:
        ix = int(x * k) + w
        iy = int(y * k) + w
        if 0 <= ix < imgw and 0 <= iy < imgw:
            img[iy][ix] += z
    z = ndi.gaussian_filter(img, (w, w))  # Gaussian convolution
    z[z <= BLANK_THRESH] = np.nan  # Making low values blank
    return z[w:-w, w:-w]


def move_coordinates_dist(point, distance, bearing):
    vince = vincenty()
    new_point = vincenty.destination(vince, point=point,
                                     bearing=bearing, distance=distance)
    dist = vincenty(point, new_point).meters

    return new_point, dist

def move_coordinates(point, distance, bearing):
    vince = vincenty()
    new_point = vincenty.destination(vince, point=point,
                                     bearing=bearing, distance=distance)
    return new_point

if __name__ == '__main__':
    center_lat = 39.228389
    center_lon = 9.109773
    center_point = Point(center_lat, center_lon)
    radius = 50 / 1000.0
    boundary = radius * 1.414
    n_nodes = 200
    moved_points = [move_coordinates(center_point,
                                     distance=boundary,
                                     bearing=(i + 1) * 10
                                     ) for i in range(n_nodes)]

    # mobile_nodes = [Node(
    #     p.latitude,
    #     p.longitude
    # ) for i, p in enumerate(moved_points)]

    lat_list = [p.latitude for p in moved_points]
    lon_list = [p.longitude for p in moved_points]

    lat_list.append(center_lat)
    lon_list.append(center_lon)

    d = {'latitude': lat_list, 'longitude': lon_list, 'color': 'red'}
    df = pd.DataFrame(data=d)
    # scatter(df['latitude'], df['longitude'], df['color'])
    scatter(df['latitude'], df['longitude'], df['color'])
