# coding=utf-8
# TODO Choose frequency for SVO, from resources list
from random import random, randrange

from matplotlib import pylab
from mobility.entities import Node
from mobility.entities.Singleton_manager import SingletonManager

import matplotlib.pyplot as plt

def frequency_assignment():

    # type: ( 'mobility.entities.Node', Node) -> list(Node)
    """
    Estimated power consumption:
    https://android.googlesource.com/platform/frameworks/base/+/master/core/res/res/xml/power_profile.xml
    https://source.android.com/devices/tech/power/index.html#power-values

    :rtype: List of resources
    :param svo_connected_list: A list of svos
    :param qos: The Quality of Service associated with the geofence
    :return: A list of resources with the frequency to set the trigger for each SVO


    Per ogni SVO serve sapere:
        E_res: la carica residua della batteria
        P_drain: quanto si sta scaricando la batteria
        E_tx: il costo energetico della trasmissione



    Coefficente alpha: INDICE DI PERTURBAZIONE DELLA TRASMISSIONE

        A parità di costo di trasmissione, più alpha è alto maggiore sarà
        l'impatto che quel task sul nodo.

                            alpha_ik = Etx_ik / Eres_i



    Coefficente delta: INDICE DI DECADIMENTO NATURALE

        All'aumentare di delta diminuisce il tempo di vita dell'intera
        rete anche in assenza di task

                            delta_ik = Pdrain_ik / Eres_i



    Coefficente phi: EQUIRIPARTIZIONE DELLE FREQUENZE

        Si divide il carico di lavoro in modo uniforma tra tutti
        i partecipanti

                            phi_k = QoS / N_k



    Coefficente beta: INDICE DI STABILITA' DI TRASMISSIONE

        All'aumentare di phi diminuiscono la perturbazione totale
        dovuta alla somma singole trasmissioni

                            beta_k = (1/N_k)sum_e(1/alpha_ek)



    Coefficente gamma: RAPPORTO DECADIMENDO PERTURBAZIONE
        il rapporto tra il battery drain e l'energia di trasmissione

                            gamma_k = (1/N_k)sum_e(delta_e/alpha_ek)



    Frequenza ottimale per ogni Nodo i e Task K: f_ik

                alpha_ik * f_ik = (phi_k/beta_k) + (gamma_k/beta_k) - delta_i

    """

    sing_man = SingletonManager()
    node_list = sing_man.get_sensing_nodes()

    F_k  = sing_man.qoi
    N_k = int(len(node_list))
    # print "N_k {}".format(N_k)
    E_res_total = float(0.0)
    E_tx_total = float(0.0)
    P_drain_total = float(0.0)
    v = 3.7

    beta_k = float(0.0)
    gamma_k = float(0.0)
    c = 0
    for fx in sing_man.fixed_nodes:
        if fx.batt_level < 0:
            fx.batt_level = 0
        c += fx.batt_level



    stopper = N_k / c

    for elem in node_list:

        p_drain = float(elem.battery_drain*v)/(3600*1000.0) # W
        e_res = float(elem.batt_level/1000.0) * 3600 * v #((mAh*1000)*v)*3600  -> J
        e_tx = float((elem.tx_cost/1000)*3600)       # (mWh/1000)*3600 -> J)

        beta_k += float(e_res / e_tx)
        gamma_k += float(p_drain / e_tx)

        E_res_total = E_res_total + e_res
        E_tx_total = E_tx_total + e_tx
        P_drain_total = P_drain_total + p_drain

    phi_k = float(F_k / N_k)
    beta_k_mean = float(beta_k / N_k)
    gamma_k_mean = float(gamma_k / N_k)

    for node in node_list:

        p_drain_n = float(node.battery_drain * v) / (3600 * 1000.0) #TODO capire unità di misura
        e_res_n = float((node.batt_level/1000.0)*3600*v) #  ((mAh*v)/3,6  -> J
        e_tx_n = float((node.tx_cost/1000.0)*3600.0)

        if e_res_n != 0:
            alpha_k_n = float(e_tx_n / e_res_n)
            delta_k_n = float(p_drain_n / e_res_n)
            f_k = float(((phi_k/beta_k_mean)+(gamma_k_mean/beta_k_mean)-delta_k_n)*(1/alpha_k_n))
        else:
            f_k = 0

        if f_k < 0:
            f_k = 0
            node.is_sensing = False
            # f_k = float(1/3600.0) #qos/(randrange(20,25,1))
            print ("Negative frequency occured!!\n")
            print ("Choosefrequency alpha_k: " + str(alpha_k_n))
            print ("Choosefrequency delta_k: " + str(delta_k_n))
            print ("Choosefrequency f_k: " + str(f_k))

        node.frequency = f_k
        node.batt_level -= node.tx_cost/20.0
        # print (node.frequency)

    return node_list

if __name__ == '__main__':


    from geopy import Point

    sing_manager = SingletonManager()
    center_lat = 39.228389
    center_lon = 9.109773
    center_point = Point(center_lat, center_lon)
    radius = 50 / 1000.0
    boundary = radius * 1.414
    n_geofence_points = 300
    n_mobile_nodes = 5
    n_fixed_nodes = 3

    sing_manager.qoi = 200 / 60.0  # Hz
    sing_manager.beta = 0.6
    sing_manager.center_point = center_point
    sing_manager.radius = radius
    sing_manager.n_fixed = n_fixed_nodes
    sing_manager.n_mobile = n_mobile_nodes

    sing_manager.generate_mobile_nodes(n_mobile_nodes, center_lat, center_lon)
    sing_manager.generate_fixed_node(10)

    are_sensing = [no.is_fixed for no in sing_manager.get_all_nodes()]



    for i, n in enumerate(sing_manager.get_all_nodes()):
        if are_sensing[i]:
            print n
            n.is_sensing = True

    frequency_assignment()

    for i in sing_manager.get_all_nodes():
        print i