import json
import urllib

import requests


def get_data_json(app_id ,limit=400, sensor_name="all"):

    url = "http://{}.appspot.com/getSensor?{}".format(app_id,sensor_name)

    limit = "{}".format(limit)

    payload = urllib.urlencode(
        {"sensorName": sensor_name, "limit": limit})
    headers = {
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",

    }

    response = requests.request("POST", url, data=payload, headers=headers)

    tx = response.text

    data = json.loads(tx)

    return data

if __name__ == '__main__':

    svo_id = "abcde-104"
    dt = get_data_json(app_id=svo_id)
    print dt