"""
Animation of Elastic collisions with Gravity

author: Jake Vanderplas
email: vanderplas@astro.washington.edu
website: http://jakevdp.github.com
license: BSD
Please feel free to use and modify this, but keep the above information. Thanks!
"""

from bokeh.models import pd
from geopy import Point

from mobility.googmaps.gmaps import move_coordinates, background_and_pixels
from mobility.googmaps.google_static_maps_api import MAPTYPE
from mobility.googmaps.google_static_maps_api import MAX_SIZE
from mobility.googmaps.google_static_maps_api import SCALE

center_lat = 39.228389
center_lon = 9.109773
center_point = Point(center_lat, center_lon)
radius = 50 / 1000.0
boundary = radius * 1.414
n_geofence_points = 300
n_mobile_nodes = 4
n_fixed_nodes = 9
maptype = MAPTYPE
width = SCALE * MAX_SIZE




import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as animation


class ParticleBox:
    """Orbits class

    init_state is an [N x 4] array, where N is the number of particles:
       [[x1, y1, vx1, vy1],
        [x2, y2, vx2, vy2],
        ...               ]

    bounds is the size of the box: [xmin, xmax, ymin, ymax]
    """

    def __init__(self,
                 init_state=[[1, 0, 0, -1],
                             [-0.5, 0.5, 0.5, 0.5],
                             [-0.5, -0.5, -0.5, 0.5]],
                 bounds=[-2, 2, -2, 2],
                 size=0.04,
                 M=0.15):
        self.init_state = np.asarray(init_state, dtype=float)
        self.M = M * np.ones(self.init_state.shape[0])
        self.size = size
        self.state = self.init_state.copy()
        self.time_elapsed = 0
        self.bounds = bounds

    def step(self, dt):
        """step once by dt seconds"""
        self.time_elapsed += dt

        # update positions
        self.state[:, :2] += dt * self.state[:, 2:]

        # crossed_x1 = (self.state[:, 0] < self.bounds[0] + self.size)
        # crossed_x2 = (self.state[:, 0] > self.bounds[1] - self.size)
        # crossed_y1 = (self.state[:, 1] < self.bounds[2] + self.size)
        # crossed_y2 = (self.state[:, 1] > self.bounds[3] - self.size)
        #
        # self.state[crossed_x1, 0] = self.bounds[0] + self.size
        # self.state[crossed_x2, 0] = self.bounds[1] - self.size
        #
        # self.state[crossed_y1, 1] = self.bounds[2] + self.size
        # self.state[crossed_y2, 1] = self.bounds[3] - self.size
        #
        # self.state[crossed_x1 | crossed_x2, 2] *= -1
        # self.state[crossed_y1 | crossed_y2, 3] *= -1


# ------------------------------------------------------------
# set up initial state
np.random.seed(0)
init_state = np.random.random((50, 4))
init_state[:, :2] *= 1200
init_state[:, 2:] -= 0.5
init_state[:, 2:] *= 100



box = ParticleBox(init_state, size=0.04)
dt = 1. / 30  # 30fps

# ------------------------------------------------------------
# set up figure and animation
fig = plt.figure()
square = [move_coordinates(center_point,
                           distance=boundary * 1.5,
                           bearing=i * 90
                           ) for i in range(4)]

lat_list = [s.latitude for s in square]
lon_list = [s.longitude for s in square]

d = {'latitude': lat_list, 'longitude': lon_list, 'colors': 'red'}
df = pd.DataFrame(data=d)
r = background_and_pixels(df['latitude'], df['longitude'], MAX_SIZE,
                          maptype)
img, pixels, zoom, size = r[0], r[1], r[2], r[3]

print(pixels)

ax = fig.add_subplot(111)
plt.gca().invert_yaxis()  # Origin of map is upper left
plt.axis([0, width, width, 0])  # Remove margin
plt.axis('off')
plt.tight_layout()
plt.imshow(np.array(img))  # Background map

# particles holds the locations of the particles
particles, = ax.plot([], [], 'bo', ms=6)

# rect is the box edge
rect = plt.Rectangle(box.bounds[::2],
                     box.bounds[1] - box.bounds[0],
                     box.bounds[3] - box.bounds[2],
                     ec='none', lw=2, fc='none')
ax.add_patch(rect)


def init():
    """initialize animation"""
    global box, rect

    particles.set_data([], [])
    rect.set_edgecolor('none')

    return particles, rect


def animate(i):
    """perform animation step"""
    global box, rect, dt, ax, fig
    box.step(dt)

    ms = 10

    # update pieces of the animation
    rect.set_edgecolor('k')
    particles.set_data(box.state[:, 0], box.state[:, 1])
    particles.set_markersize(ms)

    return particles, rect


ani = animation.FuncAnimation(fig, animate, frames=600,
                              interval=10, blit=True, init_func=init)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
# ani.save('particle_box.mp4', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()