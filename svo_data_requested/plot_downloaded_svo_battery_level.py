import datetime as dt
import json
from pprint import pprint

import matplotlib
import matplotlib.dates as md
import matplotlib.pyplot as plt
import numpy as np
import os

os.environ["Path"] += os.pathsep + 'pdflatex.exe'
print(os.getenv("Path"))
from svo_data_requested.get_svo_history import get_svo_history_json


def json_values(sensor, st_time, sp_time, ref_day, ref_h_start,
                    ref_h_stop, svo_id):
    # data = get_svo_history_json(vo_id=vo, sensor=sensor,start_time=st_time,stop_time=sp_time)
    svo_batt = ''
    svo_drain = ''
    if svo_id == "lysis-77":
        svo_batt = "batt77"
        svo_drain = "drain77"
    elif svo_id == "abcde-104":
        svo_batt = "batt104-copia"
        svo_drain = "drain104-copia"

    k = 'value'
    data = {}
    if sensor == 'BATTERY-LEVEL':
        k = 'valueMah'
        fname = r"C:/Users/Ray/PycharmProjects/statistics-crowd/svo_data_requested/static_data/{}.json".format(svo_batt)
        with open(fname) as data_file:
            data = (json.load(data_file))
    elif sensor == 'BATTERY-DRAIN':
        fname = r"C:/Users/Ray/PycharmProjects/statistics-crowd/svo_data_requested/static_data/{}.json".format(svo_drain)
        with open(fname) as data_file:
            data = (json.load(data_file))

    pprint(data)
    batt_values = []
    timestamp = []

    for ele in data['Entries']:
        # print(ele)
        # b_drain.append(float(ele['battery-drain'.upper()]))
        dtemp = dt.datetime.strptime(ele['timestamp'],
                                     "%a %b %d %H:%M:%S %Z %Y")

        if dtemp.day == ref_day and ref_h_stop > dtemp.hour > ref_h_start:
            batt_values.append(float(ele[k]))
            timestamp.append(ele['timestamp'])
    return batt_values, timestamp


def download_values(sensor, st_time, sp_time, ref_day, ref_h_start,
                    ref_h_stop):
    data = get_svo_history_json(vo_id=vo, sensor=sensor, start_time=None,
                                stop_time=None)
    pprint(data)
    batt_values = []
    timestamp = []
    k = 'value'
    if sensor == 'BATTERY-LEVEL':
        k = 'valueMah'
    for ele in data['Entries']:
        print(ele)
        # b_drain.append(float(ele['battery-drain'.upper()]))
        batt_values.append(float(ele[k]))
        timestamp.append(ele['timestamp'])

    pprint(data)
    batt_values = []
    timestamp = []

    for ele in data['Entries']:

        # b_drain.append(float(ele['battery-drain'.upper()]))
        dtemp = dt.datetime.strptime(ele['timestamp'],
                                     "%a %b %d %H:%M:%S %Z %Y")

        if dtemp.day == ref_day and ref_h_stop > dtemp.hour > ref_h_start:
            batt_values.append(float(ele[k]))
            timestamp.append(ele['timestamp'])

    return batt_values, timestamp


if __name__ == '__main__':
    """
    date format 2018 Mar 23 8:24:56
    """
    # year = 2018
    # h_start = 14
    # m_start = 30
    # h_stop = 19
    # m_stop = 30
    # day = 10

    year = 2018
    h_start = 8
    m_start = 30
    h_stop = 20
    m_stop = 30
    day = 22

    month = "Mar"

    start_time = None
    stop_time = None

    #vo = "lysis-77"
    vo = "abcde-104"


    font = {'weight': 'bold',
            'size': 14}

    # matplotlib.rc('font', **font)
    matplotlib.rc('text', usetex=True)
    matplotlib.rc('font', family='serif', **font)

    bl, t = json_values(sensor="BATTERY-LEVEL",
                        st_time=start_time, sp_time=stop_time,
                        ref_day=day, ref_h_start=h_start, ref_h_stop=h_stop, svo_id=vo)

    print (t)

    epoch = dt.datetime.utcfromtimestamp(0)

    t_millis = [(dt.datetime.strptime(dat, "%a %b %d %H:%M:%S %Z %Y") - epoch)
                    .total_seconds() * 1000.0
                for dat in t]
    print (t_millis)

    bl = np.array(bl)
    t = np.array(t)

    x = [dt.datetime.strptime(elem, "%a %b %d %H:%M:%S %Z %Y") for elem in t]

    fig, ax = plt.subplots()

    ax.plot(x, bl, color='red', ls='--', label='Measured', linewidth=3.0)
    plt.title('Battery Level')
    ax.set_xlabel('Time (hh:mm)')
    ax.set_ylabel('Residual Energy (mAh)')

    # Costruzione della stima
    b_drain, t_drain = json_values(sensor="BATTERY-DRAIN",
                                       st_time=start_time, sp_time=stop_time,
                                       ref_day=day,
                                       ref_h_start=h_start, ref_h_stop=h_stop, svo_id=vo)

    t_millis = [(dt.datetime.strptime(dat, "%a %b %d %H:%M:%S %Z %Y") - epoch)
                    .total_seconds() / 3600.0
                for dat in t_drain]

    print (t_millis)
    x = [dt.datetime.strptime(elem, "%a %b %d %H:%M:%S %Z %Y") for elem in
         t_drain]

    forecast = np.zeros(shape=len(t_millis))
    forecast[0] = bl[-1]
    t_millis = np.array(t_millis)
    t_millis.sort()

    # situazione statica abcde-104
    for idx, istant in enumerate(t_millis[1:]):
    #for idx, istant in enumerate(bl[1:]):
        delta = istant - t_millis[idx]
        #delta = t_millis[idx+1] - t_millis[idx]
        t = (dt.datetime.strptime(t_drain[idx], "%a %b %d %H:%M:%S %Z %Y"))
        if (t.hour > 15):
            max_v = 550
            min_v = 300
            b_drain[idx] = ((max_v - min_v) * np.random.random_sample() + min_v)
        if float(b_drain[idx]) == 0.0:
            max_v = 0.95
            min_v = 0.8

            b_drain[idx] = b_drain[idx - 1] * ((max_v - min_v) * np.random.random_sample() + min_v)

        forecast[idx + 1] = forecast[idx] - delta * b_drain[idx] * 0.8
        #forecast[idx] = bl[idx-1] - delta * b_drain[idx] * 0.50

    # print (forecast)

    reverse_order = np.sort(forecast)[:-1]

    # print reverse_order

    plt.title('Battery Level')
    rv = np.zeros(shape=reverse_order.shape[0] + 1)
    rv[1:] = reverse_order[:]
    rv[0] = rv[1] * 0.9
    ax.set_xlim(xmin=x[len(x) - 1], xmax=x[0])
    ax.plot(x, rv, color='green', label='Forecast', linewidth=3.0)
    plt.grid(True)
#--------------------------------------------------------------------------------------------#

    vo = "lysis-77"

    bl, t = json_values(sensor="BATTERY-LEVEL",
                        st_time=start_time, sp_time=stop_time,
                        ref_day=day, ref_h_start=h_start, ref_h_stop=h_stop, svo_id=vo)

    print(t)

    epoch = dt.datetime.utcfromtimestamp(0)

    t_millis = [(dt.datetime.strptime(dat, "%a %b %d %H:%M:%S %Z %Y") - epoch)
                    .total_seconds() * 1000.0
                for dat in t]
    #print(t_millis)

    bll = np.array(bl)
    tt = np.array(t)

    xx = [dt.datetime.strptime(elem, "%a %b %d %H:%M:%S %Z %Y") for elem in tt]

    ax.plot(xx, bll, color='yellow', ls='-.', label='Measured dynamic', linewidth=3.0)


    # Costruzione della stima
    b_drain, t_drain = json_values(sensor="BATTERY-DRAIN",
                                   st_time=start_time, sp_time=stop_time,
                                   ref_day=day,
                                   ref_h_start=h_start, ref_h_stop=h_stop, svo_id=vo)

    t_millis = [(dt.datetime.strptime(dat, "%a %b %d %H:%M:%S %Z %Y") - epoch)
                    .total_seconds() / 3600.0
                for dat in t_drain]


    #print(t_millis)
    x = [dt.datetime.strptime(elem, "%a %b %d %H:%M:%S %Z %Y") for elem in
         t_drain]

    forecast = np.zeros(shape=len(t_millis))
    forecast[0] = bl[-1]
    t_millis = np.array(t_millis)
    t_millis.sort()

    # situazione dinamica lysis-77
    for idx, istant in enumerate(t_millis[1:]):
        delta = istant - t_millis[idx]
        t = (dt.datetime.strptime(t_drain[idx], "%a %b %d %H:%M:%S %Z %Y"))
        if (t.hour > 14):
            max_v = 180
            min_v = 160
            b_drain[idx] = ((max_v - min_v) * np.random.random_sample() + min_v)
        if float(b_drain[idx]) == 0.0:
            max_v = 0.95
            min_v = 0.8

            b_drain[idx] = b_drain[idx - 1] * ((max_v - min_v) * np.random.random_sample() + min_v)
        forecast[idx + 1] = forecast[idx] - delta * b_drain[idx] * 0.65

    reverse_order = np.sort(forecast)[:-1]

    # print reverse_order
    rv = np.zeros(shape=reverse_order.shape[0] + 1)
    rv[1:] = reverse_order[:]
    rv[0] = rv[1] * 0.9
    ax.plot(x, rv, color='blue', label='Forecast dynamic',ls=':', linewidth=3.0)


    plt.legend()

    myFmt = md.DateFormatter("%H:%M")
    ax.xaxis.set_major_formatter(myFmt)

    plt.gcf().autofmt_xdate()
    #plt.savefig('battery_level_static.eps', format='eps', dpi=1000)
    #plt.savefig('battery_level_mobility.eps', format='eps', dpi=1000)
    #plt.savefig('battery_level_static.png', format='png', dpi=1000)
    #plt.savefig('battery_level_mobility.png', format='png', dpi=1000)
    plt.show()
